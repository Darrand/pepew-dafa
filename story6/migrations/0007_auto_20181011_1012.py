# Generated by Django 2.1.1 on 2018-10-11 03:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0006_auto_20181011_1011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='date',
            field=models.DateTimeField(),
        ),
    ]

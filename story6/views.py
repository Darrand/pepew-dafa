from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusUpdate
from .models import Status

response = {}
# Create your views here.
def profile(request):
    return render(request, 'profile.html')

def returned(request):
    statuses = Status.objects.all()[::-1]
    response['status_display'] = statuses
    response['status_form'] = StatusUpdate
    return render(request, 'page.html', response)

def submit(request):
    form = StatusUpdate(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['status'] = request.POST['status']
        
        status = Status(name = response['name'], status = response['status'])
        status.save()
        return HttpResponseRedirect('/story6/')
    else:
        return HttpResponseRedirect('/story6/')

$(window).load(function() {
    $('#pop-up').hide();
    $('#loading').hide();
    $('body').animate({
        'background-color' : '#00FFFF'
    })
    $('#content').show();
    $('#s').show();
    // hide the subscribers story10   
    $.ajax({
        url : '/story10/validate',
        dataType: 'json',
        success: function(result) {
            if (Object.keys(result.email).length == 0) {
                $('#sub-panel').hide()
            } else {
                showSubs()
            }
        }
    })
});

function clearQuery() {
    document.getElementById("id_name").value = ""
    document.getElementById("id_email").value = ""
    document.getElementById("id_password").value = ""
    document.getElementById("id_confirm").value = ""
    $("#email-msg").html("")
    $("#conf-msg").html("")
    $('#submit-button').attr('class', 'btn btn-default disabled')
}

var fav = 0
function shift(i){
    var star = '#star-base' + i
    if ($(star).attr('src').includes('../static/star.png')) {
        $(star).attr('src','../static/star_shine.png')
         fav++
        $('#num').html(fav)
    } else {
        $(star).attr('src','../static/star.png')
        fav--
        $('#num').html(fav)
    }
}
 $(document).ready(function(){    
        //change theme
        $( function() {
            $( "#accordion" ).accordion({
              collapsible: true,
              heightStyle: "content"
            });
        });
        var check = 0;
        $("#theme").click(function(){
            if (check == 1){
                normalTheme(); check = 0;
            } else {
                darkTheme(); check = 1;
            }
        })
        function normalTheme() {
            $('#theme').html('Change Theme');
            $('.table').animate({
                'background-color' :'#007BFF'
            })
            $('.head').animate({
                'color' : '#FFFFFF'
            })
            $('.elements').animate({
                'background-color' : '#7FFFD4'
            })
            $('.inside').animate({
                'color' : '#000000'
            })
            $('.jumbotron').animate({
                'background-color' : '#FAFAD2' 
            })
            $('.tray').animate({
                'background-color' : '#FFFFFF'
            })
            $('.username').animate({
                'background-color' : '#FFFFFF'
            })
            $('#nav-bar').animate({
                'background-color' : '#FAFAD2' 
            })
            $('.ui-accordion .ui-accordion-header').animate({
                'background-color' : '#00BFFF'
            })
            $('.ui-accordion .ui-accordion-content').animate({
                'background-color' : '#FFFFFF'
            })
            $('body').animate({
                'background-color' : '#00FFFF'
            })
            $('p').animate({
                'color' : '#000000'
            })
            $('label').animate({
                'color' : '#000000'
            })
            $('h3').animate({
                'color' : '#000000'
            })
            $('h1').animate({
                'color' : '#000000'
            })
            $('li').animate({
                'color' : '#000000'
            })
        }

        function darkTheme() {
            $('#theme').html('Change Theme');
            $('.table').animate({
                'background-color' :'#7FFFD4'
            })
            $('.head').animate({
                'color' : '#000000'
            })
            $('.elements').animate({
                'background-color' : '#007BFF'
            })
            $('.inside').animate({
                'color' : '#FFFFFF'
            })
            $('.jumbotron').animate({
                'background-color' : '#151131' 
            })
            $('#nav-bar').animate({
                'background-color' : '#151131' 
            })
            $('.ui-accordion .ui-accordion-header').animate({
                'background-color' : '#151131'
            })
            $('.ui-accordion .ui-accordion-content').animate({
                'background-color' : '#322F43'
            })
            $('.tray').animate({
                'background-color' : '#322F43'
            })
            $('.username').animate({
                'background-color' : '#322F43'
            })
            $('body').animate({
                'background-color' : '#322F43'
            })
            $('p').animate({
                'color' : '#00FFFF'
            })
            $('label').animate({
                'color' : '#00FFFF'
            })
            $('h3').animate({
                'color' : '#00FFFF'
            })
            $('h1').animate({
                'color' : '#00FFFF'
            })
            $('li').animate({
                'color' : '#00FFFF'
            })
        }
        // try story10
        // validate email
        $("#id_email").change(
            function(){
            console.log("masuk-skuy")
            var email = $(this).val();
            $.ajax({
                url : '/story10/validate',
                dataType: 'json',
                success: function(result) {
                    if(!(result.email == null)) {
                        emails = Object.keys(result.email)
                        for(var i = 0; i < emails.length; i++) {
                            if(email == emails[i]) {
                                $('#submit-button').attr('class', 'btn btn-default disabled')
                                $('#email-msg').animate({
                                    'color' : '#FF0000',          
                                    'font-weight' : 'bold'
                                })
                                $('#email-msg').html("Email has been taken")
                                return
                            }                    
                        }
                    }
                    $('#submit-button').attr('class', 'btn btn-default btn-primary')
                        $('#email-msg').animate({
                            'color' : '#008000',
                            'font-weight' : 'bold'          
                        })
                        $('#email-msg').html("Email can be used")
                }
            })
        })
        // validate password
        $("#id_confirm").keypress(function(){
            var actual_password = $("#id_password").val()
            var message = $('#conf-msg')            
            var confirmation = $(this).val()
            var conf_fix = confirmation + actual_password.slice(actual_password.length-1, actual_password.length) 
            if (conf_fix == actual_password) {
                message.animate({
                    'color' : '#008000'
                })
                message.html("Password Matches")
                $('#submit-button').attr('class', 'btn btn-default btn-primary')
            } else {
                message.animate({
                    'color' : '#FF0000'
                })
                message.html("Password doesn't match")
                $('#submit-button').attr('class', 'btn btn-default disabled')
            }
        })
        //submit 
        $('#sub-form').submit(function(event) {
            var subData = $(this).serializeArray()
            $.ajax({
                type: 'POST',
                url: 'sub',
                data: subData,
                dataType: 'json',
            }).done(function(){
                $("#end-msg").html("You have been subscribed")
                $("#end-msg").animate({
                    'color' : '#008000',
                    "font-size" : "15pt"
                })
                showSubs() 
            }).fail(function(){
                $("#end-msg").html("You might've missed something")
                $("#end-msg").animate({
                    "font-size" : "15pt"
                })
            })
            $("#sub-panel").show()
            event.preventDefault()
            clearQuery()
        })
        
        // story 9
        $.ajax({url:'/story9/api', success: function(result){
            findBook(result)
        }})
        $('#find-button').click( function(){
            var valuex = document.getElementById('find').value
            $("#body").html("");
            $.ajax({url:'/story9/api/' + valuex, success: function(result) {
                findBook(result)
            }})
        })
});

function findBook(result) {
    var search = result.items
    for (var i = 0; i < search.length; i++) {
        var volume = search[i].volumeInfo
        var image = volume.imageLinks.thumbnail
        var title = volume.title
        var author_list = volume.authors
        var author = ""
        for (var j = 0; j < author_list.length; j++) {
            author += author_list[j] + "," + "<br>" 
        }
        var sliced_author = author.slice(0, author.length - 5) 
        var desc = volume.description
        var html_block = 
        `<tr class="elements" scope="row">
        <th class="inside">`+ (i+1)+`</th>
        <td><img id="thumbnail" src=`+ image +`></td>
        <td class="inside">`+ title +`</td>
        <td class="inside">`+ sliced_author +`</td>
        <td class="inside">`+ desc +`</td>
        <td class="inside"><img id="star-base`+ i +`" onclick=shift(`+ i +`) class="star" src="../static/star.png"</td>
        </tr>
        `
        $("#body").append(html_block);
    }
}

function deleteMe(sub_id) {
    var post_id = sub_id.slice(4, sub_id.length)
    $("#pop-up").dialog({
        modal: true,
        buttons: {
            Exit: function(){
                $(this).dialog("close")
            }
        }
    })
    var bool = false
    var pass = ""
    $("#html_pass").keypress(function(){
        pass = $(this).val()
        $.ajax({
            url: '/story10/validate',
            dataType: 'json',
            success: function(result) {
                var real_password = result.email[post_id][1]
                if (pass + real_password.slice(real_password.length - 1, real_password.length) == real_password) {
                    bool = true
                }
            }        
        })
    })

    $("#conf_delete").click(function(){  
        if (bool) {
            console.log("masuk delete")
            $.ajax({
                url: 'del/'+post_id,
                dataType: 'json',
                success: function(result){
                    $("#pop-up").dialog("close")
                    showSubs()
                }
            })
        } else {
            alert("Password invalid!")
            document.getElementById("html_pass").value = ""
        }
    })
}

function showSubs() {
    $.ajax({
        url: '/story10/validate',
        dataType: 'json',
        success: function(result) {
            unique_email = Object.keys(result.email)
            var sub = ""
            for(var i = 0; i < unique_email.length; i++) {
                name = result.email[unique_email[i]][0]
                sub += `
                    <div id='`+ unique_email[i] + `'>
                    <table>
                    <td> <h3 class="col">A person named ` + name +` made a mistake </h2> </td>
                    <td> <button id='del-`+ unique_email[i] +`' class='btn btn-danger' onclick="deleteMe('del-`
                    + unique_email[i] 
                    +`')"> Redo </button> </td>
                    </table>
                    <br>
                </div>`
            }
            $('#sub-panel').html(sub)
        }
    })
}
from django import forms

class StatusUpdate(forms.Form):
    
    attrs = {"class" : "form-control"}

    name = forms.CharField(label = "name", max_length= 100, required = False ,widget = forms.TextInput(attrs=attrs))
    status = forms.CharField(label = "status", max_length= 300, required = True, widget = forms.Textarea(attrs=attrs))
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import *
from .forms import *
import time
# Create your tests here.


class Story6UnitTest(TestCase):

    def setUp(self):
        return Status.objects.create(name="example", status="lorem ipsum sekian sekian")

    def test_story6_url_status_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_story6_url_status_func_working(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, returned)

    def test_story_url_profile_is_exist(self):
        response = Client().get(reverse('profile'))
        self.assertEqual(response.status_code, 200)

    def test_story_url_profile_func_working(self):
        found = resolve(reverse('profile'))
        self.assertEqual(found.func, profile)

    def test_story6_profile_contains_correct_html(self):
        response = self.client.get(reverse('profile'))
        self.assertContains(response, '<p id="name" class="text-center"> Dafa Ramadansyah </p>')
        self.assertContains(response, '<h1>About me...</h1>')
        self.assertContains(response, 'My full name is Dafa Ramadansyah,')

    def test_story6_form_is_valid(self):
        new_form = StatusUpdate(
            data={'name': 'example', 'status': 'lorem ipsum sekian sekian'})
        self.assertTrue(new_form.is_valid())

    def test_story6_form_has_css_class(self):
        new_form = StatusUpdate()
        self.assertIn('class="form-control', new_form.as_p())

    def test_story6_form_validation_for_blank_and_offlimit_input(self):
        new_form = StatusUpdate(data={'name': '', 'status': ''})
        self.assertTrue(new_form.fields['status'].max_length == 300)
        self.assertFalse(new_form.is_valid())

    def test_story6_model_can_create_new_status(self):
        # create new status model
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_story6_model_content_is_valid(self):
        new_status = self.setUp()
        self.assertEqual(new_status.name, 'example')
        self.assertEqual(new_status.status, 'lorem ipsum sekian sekian')

    def test_story6_model_can_print(self):
        new_status = self.setUp()
        self.assertEqual('example', new_status.__str__())

    def test_story6_page_status_contains_correct_html(self):
        response = self.client.get('/story6/')
        self.assertContains(response, '<h1>Hello Apa Kabar?</h1>')
        self.assertContains(response, '<div class="jumbotron">')

    def test_story6_post_success_and_render_result(self):
        response_status = Client().post(reverse('submit'), {'name':'Anonymous','status':'lorem ipsum sekian sekian'})
        self.assertEqual(response_status.status_code,302)

        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn('Anonymous', html_response)
    
    def test_story6_post_failure_and_render_result(self):
        response_status = Client().post(reverse('submit'), {'name':'','status':''})
        self.assertEqual(response_status.status_code,302)

        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Anonymous', html_response)


class Story7FunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()
    
    def test_can_fill_and_submit_form(self):
        selenium = self.selenium

        # Opens the specified link
        selenium.get(self.live_server_url)

        # find the navbar element and navigate through the page
        status_bar = selenium.find_element_by_name('status-bar')
        status_bar.click()

        # checks if "coba-coba" is in the page or not
        self.assertNotIn('coba-coba', self.selenium.page_source)
        
        # find the 2 fields for name and Message
        name_field = selenium.find_element_by_id('id_name')
        input_field = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit-button')
        # fill the form data
        name_field.send_keys('Selenium Bot')
        input_field.send_keys('coba-coba')
        #submits the form
        submit.send_keys(Keys.RETURN)
        #checks if the form is already filled or not
        self.assertIn('coba-coba', self.selenium.page_source)

    def test_css_alignment_jumbotron_display(self):
        selenium = self.selenium;
        
        # Opens the herokuapp link
        selenium.get("http://ppw-b-dankrand.herokuapp.com/")
        # checks the landing page jumbotron layout
        landing_display = selenium.find_element_by_id('name')
        self.assertIn('Dafa Ramadansyah', landing_display.get_attribute('innerHTML'))

        # Opens the status page
        status_bar = selenium.find_element_by_name('status-bar')
        status_bar.click()

        # checks the status page jumbotron
        status_display = selenium.find_element_by_tag_name('h1') 
        self.assertIn('Hello Apa Kabar?', status_display.get_attribute('innerHTML'))


    def test_css_alignment_submit_button(self):
        selenium = self.selenium;
        
        # Opens the herokuapp link
        selenium.get("http://ppw-b-dankrand.herokuapp.com/")
        
        # Opens the status page
        status_bar = selenium.find_element_by_name('status-bar')
        status_bar.click()
        # checks the name point in the status
        submit_button = selenium.find_element_by_id('submit-button')
        self.assertIn('Post', submit_button.get_attribute('innerHTML'))
         
    def test_css_background_right_color(self):
        selenium = self.selenium;
        
        # Opens the herokuapp link
        selenium.get("http://ppw-b-dankrand.herokuapp.com/")

        # gets and checks the landing page background color
        time.sleep(5)
        background = selenium.find_element_by_tag_name('body')
        self.assertEquals('rgba(0, 255, 255, 1)', background.value_of_css_property('background-color'))
        
        # Opens the status page
        time.sleep(5)
        status_bar = selenium.find_element_by_name('status-bar')
        status_bar.click()
        
        # gets and checks the status page background color
        time.sleep(5)
        background_status = selenium.find_element_by_tag_name('body')
        self.assertEquals('rgba(0, 255, 255, 1)', background_status.value_of_css_property('background-color'))

        
    def test_css_jumbotron_right_color(self):
        selenium = self.selenium;
        
        # Opens the herokuapp link
        selenium.get("http://ppw-b-dankrand.herokuapp.com/")
        # checks the landing page jumbotron color
        jumbo_land = selenium.find_element_by_id('Home')
        self.assertEqual('rgba(250, 250, 210, 1)', jumbo_land.value_of_css_property('background-color'))
        
        # Opens the status page
        status_bar = selenium.find_element_by_name('status-bar')
        status_bar.click()

        # checks the Status page jumbotron color
        jumbo_status = selenium.find_element_by_class_name('jumbotron')
        self.assertEqual('rgba(250, 250, 210, 1)', jumbo_status.value_of_css_property('background-color'))
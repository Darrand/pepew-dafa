from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .models import *
from .forms import *
import time
# Create your views here.
response = {}

def regis(request):
    response["subscription"] = Subscription
    response["subscribers"] = Account.objects.all()
    return render(request, "subscription.html", response)

def subscribe(request):
    sub_data = Subscription(request.POST or None)
    if (request.method == 'POST' and sub_data.is_valid()):
        response['name'] = sub_data.cleaned_data['name']
        response['email'] = sub_data.cleaned_data['email']
        response['password'] = sub_data.cleaned_data['password']
        response['confirm'] = sub_data.cleaned_data['confirm']

        account = Account(name = response['name'], email = response['email']
                , password = response['password'], confirm = response['confirm'])
        account.save()
        return JsonResponse({'success': True})
    return JsonResponse({'success' : False})

def validate_account(request):
    email_dic = {}
    data = {}
    for e in Account.objects.all():
        arr = []
        arr.append(e.name)
        arr.append(e.password)  
        email_dic[e.email] = arr
    data["email"] = email_dic
    return JsonResponse(data)

def delete_account(request, email):
    issued_account = Account.objects.get(email=email)
    issued_account.delete()
    return JsonResponse({'delete'+issued_account.email :True})  
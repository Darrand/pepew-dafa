from django import forms

class Subscription(forms.Form):
    attrs = {'class' : 'form-control'}
    
    name = forms.CharField(label = "name", required=True , min_length= 3, max_length= 100, widget = forms.TextInput(attrs = attrs))
    email = forms.EmailField(label = "email", required= True, widget = forms.EmailInput(attrs = attrs))
    password = forms.CharField(label = "Password", required = True, min_length= 8, widget = forms.PasswordInput(attrs=
            {'type':'password', 'class':'form-control'}))
    confirm = forms.CharField(label = "Confirmation Password", required = True, min_length= 8, widget = forms.PasswordInput(attrs=
            {'type':'password', 'class':'form-control'}))

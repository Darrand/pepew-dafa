from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Story10UnitTest(TestCase):

    def test_story10_url_status_is_working(self):
        response = Client().get('/story10/')
        self.assertEqual(response.status_code, 200)

    def test_story9_url_status_func_working(self):
        found = resolve('/story10/')
        self.assertEqual(found.func, regis)
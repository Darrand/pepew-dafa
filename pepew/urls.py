"""pepew URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from story6.views import *
from story8.views import *
from story9.views import *
from story10.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('story6/', returned, name = "returned"),
    path('submit/', submit, name = "submit"),
    path('', profile, name = "profile"),
    path('story8/', info, name = "info"),
    path('story9/', lib, name = "library"),
    path('story9/api/', get_api, name = "api"),
    path('story9/api/<str:value>', get_api, name = "api2"),
    path('story10/', regis, name = "form_page"),
    path('story10/sub', subscribe, name = "subscribe"),
    path('story10/validate', validate_account, name = "validate"),
    path('story10/del/<str:email>', delete_account, name = "delete")  
]

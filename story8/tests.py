from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.

class Story8UnitTest(TestCase):
    def test_story8_url_status_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_story8_url_status_func_working(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, info)
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
import requests
# Create your views here
response = {}

def lib(request):
    return render(request, "libraries.html")

def get_api(request, value = "quilting"):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + value
    json_page = requests.get(url).json()
    return JsonResponse(json_page)

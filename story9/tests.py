from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.

class Story9UnitTest(TestCase):
    def test_story9_url_status_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story9_url_status_func_working(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, lib)